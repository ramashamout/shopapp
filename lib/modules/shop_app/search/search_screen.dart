import 'package:conditional_builder/conditional_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:udemy_flutter/models/shop_app/notification_model.dart';
import 'package:udemy_flutter/modules/shop_app/web_view/web_view.dart';

import '../../../layout/shop_app/cubit/cubit.dart';
import '../../../layout/shop_app/cubit/states.dart';
import '../../../shared/components/components.dart';

class NotificationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ShopCubit, ShopStates>(
      listener: (context, state) {},
      builder: (context, state) {
        return Directionality(
          textDirection: TextDirection.rtl,
          child: Scaffold(
            appBar: AppBar(
              // actions: [
              //   IconButton(
              //     icon: Icon(
              //       Icons.delete,
              //       color: Colors.red,
              //       size: 30,
              //     ),
              //     onPressed: () {
              //       ShopCubit.get(context).getDeleteNote();
              //       ShopCubit.get(context).getNotification();
              //       navigatTo(context, NotificationScreen());
              //     },
              //   ),
              // ],
              centerTitle: true,
              title: Text("الإشعارات"),
            ),
            body: ConditionalBuilder(
              condition: ShopCubit.get(context).noti != null,
              builder: (context) =>
                  builderWidget(ShopCubit.get(context).noti, context),
              fallback: (context) => Center(
                child: CircularProgressIndicator(),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget builderWidget(NotificationModel model, context) {
    return ListView.separated(
      physics: BouncingScrollPhysics(),
      itemBuilder: (context, index) =>
          buildCatItem(ShopCubit.get(context).noti.data.notif[index], context),
      separatorBuilder: (context, index) => myDivider(),
      itemCount: ShopCubit.get(context).noti.data.notif.length,
    );
  }

  Widget buildCatItem(noti model, context) => Padding(
        padding: const EdgeInsets.all(20.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 30,
              height: 30,
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: Colors.amberAccent),
              child: Icon(
                Icons.notifications_on_sharp,
                color: Colors.white,
              ),
            ),

            SizedBox(
              width: 20.0,
            ),
            Expanded(
              child: Text(
                model.body,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            // Spacer(),
            SizedBox(
              width: 5.0,
            ),
            IconButton(
              onPressed: () {
                // ShopCubit.get(context).gettype(model.name);

                navigatTo(
                    context, WebViewScreen('http://192.168.43.210:8000/login'));
              },
              icon: Icon(
                Icons.arrow_forward_ios,
              ),
            )
          ],
        ),
      );
}
