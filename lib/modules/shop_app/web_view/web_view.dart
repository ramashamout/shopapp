import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../../shared/network/local/cashe_helper.dart';
import '../../../shared/styles/colors.dart';

class WebViewScreen extends StatelessWidget {
  final String url;

  WebViewScreen(this.url);
  WebViewController controller;
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text(
              'مزاد',
              style: TextStyle(
                fontFamily: 'kack',
                color: defaultColor,
                fontSize: 24.0,
              ),
            ),
          ),
          body: WebView(
            initialUrl: url,
            onPageFinished: (_) {
              // ignore: deprecated_member_use
              controller.evaluateJavascript('''
       var email = document.getElementById("email");
       var password = document.getElementById("password");
       email.value = "${Cashehelper.sharedPreferences.get('email')}"
       password.value = "${Cashehelper.sharedPreferences.get('email')}"
        document.getElementById('customer_login').click();
       ''');
            },
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webViewController) {
              controller = webViewController;
            },
          )),
    );
  }
}
