import 'package:flutter/material.dart';
import 'package:udemy_flutter/modules/shop_app/profile/profile_screen.dart';
import 'package:udemy_flutter/shared/components/components.dart';

import '../../../shared/components/constants.dart';
import '../../../shared/styles/colors.dart';

class SettingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: [
      Container(
        height: 250.0,
        color: defaultColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image(
              image: AssetImage('assets/images/setting.png'),
              height: 90.0,
              width: 90.0,
            ),
            // SizedBox(
            //   height: 5.0,
            // ),
            Text(
              'الإعدادات',
              style: Theme.of(context)
                  .textTheme
                  .headline4
                  .copyWith(color: Colors.white54),
            ),
          ],
        ),
      ),
      Column(
        children: [
          ListTile(
            leading: Icon(Icons.person),
            title: Text('الملف الشخصي'),
            onTap: () {
              navigatTo(context, profile_screen());
            },
          ),
          // ListTile(
          //   leading: Icon(Icons.calendar_month_outlined),
          //   title: Text('حجز موعد'),
          //   onTap: () {
          //     navigatTo(context, FavritesScreen());
          //   },
          // ),
          ListTile(
            leading: Icon(Icons.logout),
            title: Text('تسجيل الخروج'),
            onTap: () {
              signOut(context);
            },
          ),
        ],
      )
    ]));
  }
}
