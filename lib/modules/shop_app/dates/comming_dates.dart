import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../layout/shop_app/cubit/cubit.dart';
import '../../../layout/shop_app/cubit/states.dart';
import '../../../models/shop_app/comming_appointment.dart';
import '../../../shared/components/components.dart';

class CommingDates extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ShopCubit, ShopStates>(
      listener: (context, state) {},
      builder: (context, state) {
        return Directionality(
          textDirection: TextDirection.rtl,
          child: Scaffold(
              appBar: AppBar(),
              body: ListView.separated(
                itemBuilder: (context, index) => builderWidget(
                    ShopCubit.get(context).commingapp.data[index], context),
                separatorBuilder: (context, index) => myDivider(),
                itemCount: ShopCubit.get(context).commingapp.data.length,
              )),
        );
      },
    );
  }

  Widget builderWidget(DataModel model, context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ExpansionTile(
            title: Text('رقم الموعد'),
            subtitle: Text('${model.id}'),
            controlAffinity: ListTileControlAffinity.leading,
            children: [
              ListTile(
                leading: Image(
                  image: AssetImage('assets/images/calendar (1).png'),
                  width: 25.0,
                  height: 25.0,
                ),
                title: Text('تاريخ الموعد :'),
                subtitle: Text('${model.date}'),
              ),
              ListTile(
                leading: Image(
                  image: AssetImage('assets/images/clock (1).png'),
                  width: 25.0,
                  height: 25.0,
                ),
                title: Text('وقت الموعد :'),
                subtitle: Text('${model.time}'),
              ),
              ListTile(
                leading: Image(
                  image: AssetImage('assets/images/calendar (2).png'),
                  width: 25.0,
                  height: 25.0,
                ),
                title: Text('حالة الموعد :'),
                subtitle: Text('${model.statue}'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
