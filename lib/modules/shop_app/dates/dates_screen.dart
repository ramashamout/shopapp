// ignore_for_file: deprecated_member_use

import 'package:conditional_builder/conditional_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:table_calendar/table_calendar.dart';

import '../../../layout/shop_app/cubit/cubit.dart';
import '../../../layout/shop_app/cubit/states.dart';

class DatesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ShopCubit, ShopStates>(
      listener: (context, state) {
        // TODO: implement listener
      },
      builder: (context, state) {
        return SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                TableCalendar(
                  firstDay: DateTime.now(),
                  lastDay: DateTime.utc(2030, 3, 14),
                  focusedDay: DateTime.now(),
                  selectedDayPredicate: (day) {
                    return isSameDay(ShopCubit.get(context).selected_Day, day);
                  },
                  onDaySelected: (selectedDay, focusedDay) {
                    if (!isSameDay(
                        ShopCubit.get(context).selected_Day, selectedDay)) {
                      ShopCubit.get(context)
                          .changeDate(selectedDay, focusedDay);

                      // ShopCubit.get(context).postDate();
                    }
                  },
                  onPageChanged: (focusedDay) {
                    // No need to call `setState()` here
                    ShopCubit.get(context).focused_Day = focusedDay;
                  },
                  calendarFormat: ShopCubit.get(context).calendar_Format,
                  onFormatChanged: (format) {
                    ShopCubit.get(context).changeCalendarFormat(format);
                  },
                ),
                SizedBox(
                  height: 20.0,
                ),
                ConditionalBuilder(
                  condition: ShopCubit.get(context).selected_Day != null,
                  builder: (context) => Container(
                      child: GridView.count(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    crossAxisCount: 2,
                    childAspectRatio: 2.0,
                    children: List.generate(
                      ShopCubit.get(context).bookedModel.data.length,
                      (index) => TimingsData(
                          ShopCubit.get(context).bookedModel.data[index],
                          context,
                          index),
                    ),
                  )),
                  fallback: (context) => Center(
                    child: CircularProgressIndicator(),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget TimingsData(time, context, index) => Padding(
        padding: const EdgeInsets.all(0.0),
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              FloatingActionButton.extended(
                backgroundColor:
                    ShopCubit.get(context).bookedModel.data[index] != null
                        ? Colors.amber[300]
                        : Colors.grey,
                onPressed: () {
                  ShopCubit.get(context).changetime();
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return Expanded(
                          child: AlertDialog(
                            // title: Text(''),
                            content: Text(
                              'هل تريد تثبيت الموعد ؟',
                              textDirection: TextDirection.rtl,
                            ),
                            actions: [
                              FlatButton(
                                textColor: Colors.black,
                                onPressed: () => Navigator.pop(context, false),
                                child: Text('لا'),
                              ),
                              FlatButton(
                                textColor: Colors.black,
                                onPressed: () => Navigator.pop(context, true),
                                child: Text('نعم'),
                              ),
                            ],
                          ),
                        );
                      }).then((value) => {
                        if (value)
                          {
                            ShopCubit.get(context).creatAppoitment(time),
                            ShopCubit.get(context).postDate(),
                          }
                      });
                },
                icon: Icon(
                  Icons.access_time,
                  color: Colors.black87,
                ),
                label: Text(
                  time,
                  style: TextStyle(
                    color: Colors.black87,
                  ),
                ),
              )
            ],
          ),
        ),
      );
}
