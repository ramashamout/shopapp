import 'package:conditional_builder/conditional_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:udemy_flutter/layout/shop_app/cubit/cubit.dart';
import 'package:udemy_flutter/layout/shop_app/cubit/states.dart';
import 'package:udemy_flutter/shared/components/components.dart';

import '../../../shared/components/constants.dart';

class profile_screen extends StatelessWidget {
  @override
  var nameController = TextEditingController();

  var phoneController = TextEditingController();
  var formKey = GlobalKey<FormState>();
  Widget build(BuildContext context) {
    return BlocConsumer<ShopCubit, ShopStates>(
      listener: (context, state) {},
      builder: (context, state) {
        var model = ShopCubit.get(context).userModel;
        nameController.text = model.data.user.name;
        phoneController.text = model.data.user.mobileNumber.toString();

        return ConditionalBuilder(
          condition: ShopCubit.get(context).userModel != null,
          builder: (context) => Padding(
            padding: const EdgeInsets.all(8.0),
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Form(
                key: formKey,
                child: Column(
                  children: [
                    Container(
                      height: 190.0,
                      child: Stack(
                        alignment: AlignmentDirectional.bottomCenter,
                        children: [
                          Align(
                            child: Container(
                              height: 140.0,
                              width: double.infinity,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(
                                    4.0,
                                  ),
                                  topRight: Radius.circular(
                                    4.0,
                                  ),
                                ),
                                image: DecorationImage(
                                  image: AssetImage(
                                      'assets/images/background.jpg'),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                            alignment: AlignmentDirectional.topCenter,
                          ),
                          CircleAvatar(
                            radius: 64.0,
                            backgroundColor:
                                Theme.of(context).scaffoldBackgroundColor,
                            child: CircleAvatar(
                              radius: 60.0,
                              backgroundImage:
                                  AssetImage('assets/images/profile.jpg'),
                            ),
                          ),
                        ],
                      ),
                    ),
                    // Padding(
                    //   padding: const EdgeInsets.all(20.0),
                    //   child: Container(
                    //     child: Column(children: [
                    //       defaultFormField(
                    //           controller: nameController,
                    //           type: TextInputType.name,
                    //           validate: (String value) {
                    //             if (value.isEmpty)
                    //               return ' الرجاء إدخال الإسم  ';
                    //           },
                    //           label: 'الإسم',
                    //           prefix: Icons.person),
                    //       SizedBox(
                    //         height: 15,
                    //       ),
                    //       defaultFormField(
                    //           controller: phoneController,
                    //           type: TextInputType.name,
                    //           validate: (String value) {
                    //             if (value.isEmpty)
                    //               return ' الرجاء إدخال رقم الموبايل  ';
                    //           },
                    //           label: 'رقم الموبايل',
                    //           prefix: Icons.person),
                    //       SizedBox(
                    //         height: 20,
                    //       ),
                    //       // defaultButton(
                    //       //   function: () {
                    //       //     signOut(context);
                    //       //   },
                    //       //   text: 'Logout',
                    //       // ),
                    //       // SizedBox(
                    //       //   height: 20,
                    //       // ),
                    //       // ListTile(
                    //       //   leading: Icon(Icons.logout),
                    //       //   title: Text('تسجيل الخروج'),
                    //       //   onTap: () {
                    //       //     signOut(context);
                    //       //   },
                    //       // ),
                    //       SizedBox(
                    //         height: 15,
                    //       ),
                    //       // ListTile(
                    //       //   leading: Icon(Icons.update),
                    //       //   title: Text('تعديل المعلومات'),
                    //       //   onTap: () {},
                    //       // ),
                    //       defaultButton(
                    //         function: () {
                    //           if (formKey.currentState.validate()) {
                    //             ShopCubit.get(context).updateUserData(
                    //               name: nameController.text,
                    //               phone: phoneController.text,
                    //             );
                    //           }
                    //         },
                    //         text: 'تعديل المعلومات',
                    //       ),
                    //       SizedBox(
                    //         height: 15,
                    //       ),

                    Column(
                      children: <Widget>[
                        const ExpansionTile(
                          title: Text('ExpansionTile 3'),
                          subtitle: Text('Leading expansion arrow icon'),
                          controlAffinity: ListTileControlAffinity.leading,
                          children: <Widget>[
                            ListTile(title: Text('This is tile number 3')),
                          ],
                        ),
                      ],
                    ),

                    defaultButton(
                      function: () {
                        signOut(context);
                      },
                      text: 'تسجيل الخروج',
                    ),
                    //     ]),
                    //   ),
                    // ),
                  ],
                ),
              ),
            ),
          ),
          fallback: (context) => Center(child: CircularProgressIndicator()),
        );
      },
    );
  }
}
