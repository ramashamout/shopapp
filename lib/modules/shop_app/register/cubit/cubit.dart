import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:udemy_flutter/models/shop_app/register_model.dart';
import 'package:udemy_flutter/modules/shop_app/register/cubit/states.dart';
import 'package:udemy_flutter/shared/components/constants.dart';
import 'package:udemy_flutter/shared/network/end_points.dart';
import 'package:udemy_flutter/shared/network/remote/dio_helper.dart';

import '../../../../models/shop_app/verification_model.dart';

class ShopRegisterCubit extends Cubit<ShopRegisterStates> {
  ShopRegisterCubit() : super(ShopRegisterInitialState());

  static ShopRegisterCubit get(context) => BlocProvider.of(context);

  ShopRegisterModel registerModel;

  void userRegister({
    @required String name,
    @required String email,
    @required String phone,
    @required String password,
    @required String confirm,
  }) {
    emit(ShopRegisterLoadingState());

    DioHelper.postData(
      url: REGISTER,
      data: {
        'name': name,
        'email': email,
        'password': password,
        'password_confirmation': confirm,
        'mobile_number': phone,
      },
    ).then((value) {
      print(value.data);
      registerModel = ShopRegisterModel.fromJson(value.data);
      emit(ShopRegisterSuccessState(registerModel));
    }).catchError((error) {
      print(error.toString());
      emit(ShopRegisterErrorState(error.toString()));
    });
  }

  VerificationModel model;
  void verifcation({
    @required String code,
  }) {
    emit(ShopverifcationLoadingState());

    DioHelper.postData(
      url: VERIFY,
      token: token1,
      data: {
        'code': code,
      },
    ).then((value) {
      print(value.data);
      model = VerificationModel.fromJson(value.data);
      print(model.message);
      emit(ShopverifcationSuccessState(model));
    }).catchError((error) {
      print(error.toString());
      emit(ShopverifcationErrorState(error.toString()));
    });
  }

  IconData suffix = Icons.visibility_outlined;
  bool isPassword = true;

  void changePasswordVisibility() {
    isPassword = !isPassword;
    suffix =
        isPassword ? Icons.visibility_outlined : Icons.visibility_off_outlined;

    emit(ShopRegisterChangePasswordVisibilityState());
  }
}
