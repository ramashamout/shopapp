import 'package:udemy_flutter/models/shop_app/register_model.dart';
import 'package:udemy_flutter/models/shop_app/verification_model.dart';

abstract class ShopRegisterStates {}

class ShopRegisterInitialState extends ShopRegisterStates {}

class ShopRegisterLoadingState extends ShopRegisterStates {}

class ShopRegisterSuccessState extends ShopRegisterStates {
  final ShopRegisterModel Model;

  ShopRegisterSuccessState(this.Model);
}

class ShopRegisterErrorState extends ShopRegisterStates {
  final String error;

  ShopRegisterErrorState(this.error);
}

class ShopverifcationLoadingState extends ShopRegisterStates {}

class ShopverifcationSuccessState extends ShopRegisterStates {
  final VerificationModel model;

  ShopverifcationSuccessState(this.model);
}

class ShopverifcationErrorState extends ShopRegisterStates {
  final String error;

  ShopverifcationErrorState(this.error);
}

class ShopRegisterChangePasswordVisibilityState extends ShopRegisterStates {}
