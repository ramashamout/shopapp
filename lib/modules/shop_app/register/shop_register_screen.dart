import 'package:conditional_builder/conditional_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:udemy_flutter/modules/shop_app/register/cubit/states.dart';
import 'package:udemy_flutter/modules/shop_app/register/verifyEmail_screen.dart';
import 'package:udemy_flutter/shared/network/local/cashe_helper.dart';

import '../../../shared/components/components.dart';
import 'cubit/cubit.dart';

class ShopRegisterScreen extends StatelessWidget {
  @override
  var nameController = TextEditingController();

  var emailController = TextEditingController();
  var phoneController = TextEditingController();
  var passwordController = TextEditingController();
  var confirmPasswordController = TextEditingController();
  var formKey = GlobalKey<FormState>();
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ShopRegisterCubit(),
      child: BlocConsumer<ShopRegisterCubit, ShopRegisterStates>(
        listener: (context, state) {
          if (state is ShopRegisterSuccessState) {
            if (state.Model.status) {
              print(state.Model.message);

              showToast(
                text: state.Model.message,
                state: ToastStates.SUCCESS,
              );
              Cashehelper.saveData(
                key: 'token1',
                value: state.Model.data.token,
              ).then((value) {
                // token1 = state.Model.data.token;

                navigatAndfinish(
                  context,
                  VerifyEmailScreen(),
                );
              });
            } else {
              print(state.Model.message);
              showToast(
                text: state.Model.message,
                state: ToastStates.ERROR,
              );
            }
          }
        },
        builder: (context, state) {
          return Directionality(
            textDirection: TextDirection.rtl,
            child: Scaffold(
              appBar: AppBar(),
              body: Center(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Form(
                      key: formKey,
                      child: Column(children: [
                        Text(
                          'إنشاء حساب جديد',
                          style: Theme.of(context)
                              .textTheme
                              .headline4
                              .copyWith(color: Colors.black),
                        ),
                        SizedBox(
                          height: 30.0,
                        ),
                        defaultFormField(
                            controller: nameController,
                            type: TextInputType.name,
                            validate: (String value) {
                              if (value.isEmpty) return ' الرجاء إدخال الإسم  ';
                            },
                            label: 'الإسم',
                            prefix: Icons.person),
                        SizedBox(
                          height: 15.0,
                        ),
                        defaultFormField(
                            controller: emailController,
                            type: TextInputType.emailAddress,
                            validate: (String value) {
                              if (value.isEmpty)
                                return ' الرجاء إدخال البريد الإلكتروني ';
                            },
                            label: '  البريد الإلكتروني ',
                            prefix: Icons.email),
                        SizedBox(
                          height: 15.0,
                        ),
                        defaultFormField(
                            controller: passwordController,
                            suffix: ShopRegisterCubit.get(context).suffix,
                            type: TextInputType.visiblePassword,
                            onSubmit: (value) {},
                            isPassword:
                                ShopRegisterCubit.get(context).isPassword,
                            suffixPressed: () {
                              ShopRegisterCubit.get(context)
                                  .changePasswordVisibility();
                            },
                            validate: (String value) {
                              if (value.isEmpty)
                                return ' كلمة المرور قصيرة جداً  ';
                            },
                            label: ' كلمة المرور ',
                            prefix: Icons.lock_outline),
                        SizedBox(
                          height: 15.0,
                        ),
                        defaultFormField(
                            controller: confirmPasswordController,
                            suffix: ShopRegisterCubit.get(context).suffix,
                            type: TextInputType.visiblePassword,
                            onSubmit: (value) {},
                            isPassword:
                                ShopRegisterCubit.get(context).isPassword,
                            suffixPressed: () {
                              ShopRegisterCubit.get(context)
                                  .changePasswordVisibility();
                            },
                            validate: (String value) {
                              if (value.isEmpty)
                                return ' كلمة المرور قصيرة جداً  ';
                            },
                            label: 'تأكيد كلمة المرور',
                            prefix: Icons.lock_outline),
                        SizedBox(
                          height: 15.0,
                        ),
                        defaultFormField(
                            controller: phoneController,
                            type: TextInputType.name,
                            validate: (String value) {
                              if (value.isEmpty)
                                return ' الرجاء إدخال رقم الموبايل  ';
                            },
                            label: 'رقم الموبايل',
                            prefix: Icons.person),
                        SizedBox(
                          height: 30.0,
                        ),
                        ConditionalBuilder(
                          condition: state is! ShopRegisterLoadingState,
                          builder: (context) => defaultButton(
                            function: () {
                              if (formKey.currentState.validate()) {
                                ShopRegisterCubit.get(context).userRegister(
                                  name: nameController.text,
                                  phone: phoneController.text,
                                  email: emailController.text,
                                  password: passwordController.text,
                                  confirm: confirmPasswordController.text,
                                );
                              }
                            },
                            text: 'إنشاء حساب',
                            isUpperCase: true,
                          ),
                          fallback: (context) =>
                              Center(child: CircularProgressIndicator()),
                        ),
                      ]),
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
