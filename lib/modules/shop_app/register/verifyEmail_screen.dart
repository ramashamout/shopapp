import 'package:conditional_builder/conditional_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:udemy_flutter/modules/shop_app/login/shop_login_screen.dart';
import 'package:udemy_flutter/modules/shop_app/register/cubit/states.dart';

import '../../../shared/components/components.dart';
import 'cubit/cubit.dart';

class VerifyEmailScreen extends StatelessWidget {
  var CodeController = TextEditingController();
  var formKeyVerification = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ShopRegisterCubit(),
      child: BlocConsumer<ShopRegisterCubit, ShopRegisterStates>(
        listener: (context, state) {
          if (state is ShopverifcationSuccessState) {
            if (state.model.status) {
              print(state.model.message);

              showToast(
                text: state.model.message,
                state: ToastStates.SUCCESS,
              );

              navigatAndfinish(
                context,
                ShopLoginScreen(),
              );
            } else {
              print(state.model.message);
              showToast(
                text: state.model.message,
                state: ToastStates.ERROR,
              );
            }
          }
        },
        builder: (context, state) {
          return Directionality(
            textDirection: TextDirection.rtl,
            child: Scaffold(
              appBar: AppBar(),
              body: Center(
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Form(
                      key: formKeyVerification,
                      child: Column(children: [
                        Text(
                          'التحقق من البريد ',
                          style: Theme.of(context)
                              .textTheme
                              .headline4
                              .copyWith(color: Colors.black),
                        ),
                        SizedBox(
                          height: 30.0,
                        ),
                        defaultFormField(
                            controller: CodeController,
                            suffix: ShopRegisterCubit.get(context).suffix,
                            type: TextInputType.visiblePassword,
                            onSubmit: (value) {},
                            isPassword:
                                ShopRegisterCubit.get(context).isPassword,
                            suffixPressed: () {
                              ShopRegisterCubit.get(context)
                                  .changePasswordVisibility();
                            },
                            validate: (String value) {
                              if (value.isEmpty) return 'الكود قصير جداً';
                            },
                            label: 'كود التحقق',
                            prefix: Icons.lock_outline),
                        SizedBox(
                          height: 30.0,
                        ),
                        ConditionalBuilder(
                          condition: state is! ShopverifcationLoadingState,
                          builder: (context) => defaultButton(
                            function: () {
                              if (formKeyVerification.currentState.validate()) {
                                ShopRegisterCubit.get(context).verifcation(
                                  code: CodeController.text,
                                );
                              }
                            },
                            text: 'إنشاء حساب',
                            isUpperCase: true,
                          ),
                          fallback: (context) =>
                              Center(child: CircularProgressIndicator()),
                        ),
                      ]),
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
