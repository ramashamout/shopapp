import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:udemy_flutter/layout/shop_app/cubit/cubit.dart';
import 'package:udemy_flutter/layout/shop_app/cubit/states.dart';
import 'package:udemy_flutter/modules/shop_app/web_view/web_view.dart';
import 'package:udemy_flutter/shared/components/components.dart';

import '../../../shared/components/constants.dart';
import '../dates/comming_dates.dart';

class profile_screen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ShopCubit, ShopStates>(
      listener: (context, state) {},
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              const ExpansionTile(
                title: Text('معلومات الحساب'),
                // subtitle: Text('Leading expansion arrow icon'),
                controlAffinity: ListTileControlAffinity.leading,
                children: [
                  ListTile(
                    // leading: Icon(Icons.email),
                    leading: Image(
                      image: AssetImage('assets/images/gmail.png'),
                      width: 28.0,
                      height: 28.0,
                    ),
                    title: Text('البريد الإلكتروني :'),
                    subtitle: Text('ramashamoot98@gmail.com'),
                  ),
                  ListTile(
                    // leading: Icon(Icons.person),
                    leading: Image(
                      image: AssetImage('assets/images/user.png'),
                      width: 28.0,
                      height: 28.0,
                    ),
                    title: Text('الإسم:'),
                    subtitle: Text('راما شموط'),
                  ),
                  ListTile(
                    // leading: Icon(Icons.phone),
                    leading: Image(
                      image: AssetImage('assets/images/smartphone.png'),
                      width: 28.0,
                      height: 28.0,
                    ),
                    title: Text('رقم الهاتف:'),
                    subtitle: Text('0981320790'),
                  ),
                ],
              ),
              ListTile(
                  leading: Image(
                    image: AssetImage('assets/images/appointment.png'),
                    width: 28.0,
                    height: 28.0,
                  ),
                  title: Text('حجوزاتي'),
                  trailing: IconButton(
                    onPressed: () {
                      // ShopCubit.get(context).gettype(model.name);
                      ShopCubit.get(context).getCommApp();
                      navigatTo(context, CommingDates());
                    },
                    icon: Icon(
                      Icons.arrow_forward_ios,
                    ),
                  )),
              ListTile(
                  leading: Image(
                    image: AssetImage('assets/images/auction (1).png'),
                    width: 23.0,
                    height: 23.0,
                  ),
                  title: Text(' زيارة موقع المزاد'),
                  trailing: IconButton(
                    onPressed: () {
                      // ShopCubit.get(context).gettype(model.name);

                      navigatTo(context,
                          WebViewScreen('http://192.168.43.210:8000/login'));
                    },
                    icon: Icon(
                      Icons.arrow_forward_ios,
                    ),
                  )),

              // const ExpansionTile(
              //   title: Text('حجوزاتي'),
              //   // subtitle: Text('Leading expansion arrow icon'),
              //   controlAffinity: ListTileControlAffinity.leading,
              //   children: [
              //     ListTile(
              //       leading: Icon(Icons.calendar_month),
              //       title: Text('تاريخ الموعد :'),
              //       subtitle: Text('22-7-2021'),
              //     ),
              //     ListTile(
              //       leading: Icon(Icons.access_time),
              //       title: Text('وقت الموعد :'),
              //       subtitle: Text('08:00'),
              //     ),
              //     ListTile(
              //       leading: Icon(Icons.timelapse),
              //       title: Text('حالة الموعد :'),
              //       subtitle: Text('قيد الإنتظار'),
              //     ),
              //   ],
              // ),
              SizedBox(
                height: 8,
              ),
              defaultButton(
                function: () {
                  signOut(context);
                },
                text: 'تسجيل الخروج',
              ),
            ],
          ),
        );
      },
    );
  }
}
