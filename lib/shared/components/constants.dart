import '../../modules/shop_app/login/shop_login_screen.dart';
import '../network/local/cashe_helper.dart';
import 'components.dart';

void signOut(context) {
  Cashehelper.removeData(key: 'token').then((value) {
    if (value) {
      Cashehelper.removeData(key: 'email');
      Cashehelper.removeData(key: 'password');
      navigatAndfinish(context, ShopLoginScreen());
    }
  });
}

void printFullText(String text) {
  final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
  pattern.allMatches(text).forEach((match) => print(match.group(0)));
}

String token = Cashehelper.getData(key: 'token');
String token1 = Cashehelper.getData(key: 'token1');
String tokenFcm = Cashehelper.getData(key: 'tokenfcm');
String email = Cashehelper.getData(key: 'email');
String password = Cashehelper.getData(key: 'password');
