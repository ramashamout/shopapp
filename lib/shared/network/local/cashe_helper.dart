import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Cashehelper {
  static SharedPreferences sharedPreferences;
  static init() async {
    sharedPreferences = await SharedPreferences.getInstance();
  }

// //////objectlist
  static Future<bool> putObjectList(String key, List<Object> list) async {
    // if (prefs == null) return null;
    List<String> _dataList = list?.map((value) {
      return json.encode(value);
    })?.toList();
    return sharedPreferences.setStringList(key, _dataList);
  }

  static List<Map> getObjectList(String key) {
    // if (prefs == null) return null;
    List<String> dataList = sharedPreferences.getStringList(key);
    return dataList?.map((value) {
      Map _dataMap = json.decode(value);
      return _dataMap;
    })?.toList();
  }

  static List<T> getObjList<T>(String key, T f(Map v),
      {List<T> defValue = const []}) {
    // if (prefs == null) return null;
    List<Map> dataList = getObjectList(key);
    List<T> list = dataList?.map((value) {
      return f(value);
    })?.toList();
    return list ?? defValue;
  }

// //////endobjectlist
// //////stringlist
  static Future<bool> putStringList(String key, List<String> list) async {
    return await sharedPreferences.setStringList(key, list);
  }

  static List<String> getStringList({@required String key}) {
    return sharedPreferences.getStringList(key);
  }

  static Future<bool> putBoolen({
    @required String key,
    @required bool value,
  }) async {
    return await sharedPreferences.setBool(key, value);
  }

  static dynamic getData({
    @required String key,
  }) {
    return sharedPreferences.get(key);
  }

  static dynamic getobject({
    @required String key,
  }) {
    return json.decode(sharedPreferences.getString(key));
  }

  static Future<bool> saveData({
    @required String key,
    @required dynamic value,
  }) async {
    if (value is String) return await sharedPreferences.setString(key, value);
    if (value is int) return await sharedPreferences.setInt(key, value);
    if (value is bool) return await sharedPreferences.setBool(key, value);
    if (value is double) return await sharedPreferences.setDouble(key, value);
    return await sharedPreferences.setString(key, json.encode(value));
  }

  static Future<bool> removeData({
    @required String key,
  }) async {
    return await sharedPreferences.remove(key);
  }

  // static Future<bool> saveDataNot({
  //   @required dynamic title,
  //   @required dynamic body,
  // }) async {
  //   Map<String, dynamic> notification = {'title': title, 'body': body};

  //   bool result = await sharedPreferences.setString(
  //       'notification', jsonEncode(notification));
  // }

  // static dynamic getDataNot() {
  //   String userPref = sharedPreferences.getString('notification');

  //   Map<String, dynamic> userMap = jsonDecode(userPref) as Map<String, dynamic>;
  //   return userMap;
  // }

  // static dynamic read(String key) async {
  //   final prefs = await SharedPreferences.getInstance();
  //   return json.decode(prefs.getString(key));
  // }

  // static Future<bool> savenot(String key, value) async {
  //   final prefs = await SharedPreferences.getInstance();
  //   prefs.setString(key, json.encode(value));
  // }

  // remove(String key) async {
  //   final prefs = await SharedPreferences.getInstance();
  //   prefs.remove(key);
  // }
}
