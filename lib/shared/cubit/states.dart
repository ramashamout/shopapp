abstract class Appstates {}

class AppInitialState extends Appstates {}

class AppChangeBottomNavBarState extends Appstates {}

class AppCreateDatabaseState extends Appstates {}

class AppGetDatabaseState extends Appstates {}

class AppInsertDatabaseState extends Appstates {}

class AppChangeBottomState extends Appstates {}

class AppGetDatabaseLoadingState extends Appstates {}

class AppUpdateDatabaseState extends Appstates {}

class AppDeleteDatabaseState extends Appstates {}

class AppChangeModeState extends Appstates {}
