import 'package:udemy_flutter/models/shop_app/change_favorites_model.dart';
import 'package:udemy_flutter/models/shop_app/login_model.dart';

abstract class ShopStates {}

class ShopInitialState extends ShopStates {}

class ShopChangeBottomNavState extends ShopStates {}

class ShopChangecolaState extends ShopStates {}

class ShopLoadingHomeDataState extends ShopStates {}

class ShopSuccessHomeDataState extends ShopStates {}

class ShopErrorHomeDataState extends ShopStates {}

class ShopSuccessCategoriesState extends ShopStates {}

class ShopErrorCategoriesState extends ShopStates {}

class ShopLoadingTypeProductsState extends ShopStates {}

class ShopSuccessTypeProductsState extends ShopStates {}

class ShopErrorTypeProductsState extends ShopStates {}

class ShopChangeFavoritesState extends ShopStates {}

class ShopSuccessChangeFavoritesState extends ShopStates {
  final ChangeFavoritesModel model;

  ShopSuccessChangeFavoritesState(this.model);
}

class ShopErrorChangeFavoritesState extends ShopStates {}

class ShopLoadingGetFavoritesState extends ShopStates {}

class ShopSuccessGetFavoritesState extends ShopStates {}

class ShopErrorGetFavoritesState extends ShopStates {}

class ShopLoadingGetpState extends ShopStates {}

class ShopSuccessGetpState extends ShopStates {}

class ShopErrorGetpState extends ShopStates {}

class ShopLoadingUserDataState extends ShopStates {}

class ShopSuccessUserDataState extends ShopStates {
  final ShopLoginModel loginModel;

  ShopSuccessUserDataState(this.loginModel);
}

class ShopErrorUserDataState extends ShopStates {}

class ShopLoadingUpdateUserState extends ShopStates {}

class ShopSuccessUpdateUserState extends ShopStates {
  final ShopLoginModel loginModel;

  ShopSuccessUpdateUserState(this.loginModel);
}

class ShopErrorUpdateUserState extends ShopStates {}

class ShopLoadingDatesState extends ShopStates {}

class ShopSuccessDatesState extends ShopStates {}

class ShopErrorDatesState extends ShopStates {}

class ShopEnterDaypickerState extends ShopStates {}

class ShopCalendarFormatState extends ShopStates {}

class ShopLoadingGetDateState extends ShopStates {}

class ShopSuccessGetDateState extends ShopStates {}

class ShopErrorGetDateState extends ShopStates {}

class ShopLoadingBookingState extends ShopStates {}

class ShopSuccessBookingState extends ShopStates {}

class ShopErrorBookingState extends ShopStates {}

class ShopChangeTimeState extends ShopStates {}

class ShopLoadingDeviceTokenState extends ShopStates {}

class ShopSuccessDeviceTokenState extends ShopStates {}

class ShopErrorDeviceTokenState extends ShopStates {}

class ShopLoadingNotificationState extends ShopStates {}

class ShopSuccessNotificationState extends ShopStates {}

class ShopErrorNotificationState extends ShopStates {}

class ShopLoadingDeleteNotificationState extends ShopStates {}

class ShopSuccessDeleteNotificationState extends ShopStates {}

class ShopErrorDeleteNotificationState extends ShopStates {}

class ShopLoadingCommingAppState extends ShopStates {}

class ShopSuccessCommingAppState extends ShopStates {}

class ShopErrorCommingAppState extends ShopStates {}
