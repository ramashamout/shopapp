import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:udemy_flutter/layout/shop_app/cubit/states.dart';
import 'package:udemy_flutter/models/shop_app/categories_model.dart';
import 'package:udemy_flutter/models/shop_app/change_favorites_model.dart';
import 'package:udemy_flutter/models/shop_app/favorites_model.dart';
import 'package:udemy_flutter/models/shop_app/login_model.dart';
import 'package:udemy_flutter/models/shop_app/notification_model.dart';

import 'package:udemy_flutter/modules/shop_app/cateogries/categories_screen.dart';
import 'package:udemy_flutter/modules/shop_app/dates/dates_screen.dart';
import 'package:udemy_flutter/modules/shop_app/favorites/favorites_screen.dart';
import 'package:udemy_flutter/modules/shop_app/home/home_screen.dart';

import 'package:udemy_flutter/modules/shop_app/profile/profile_screen.dart';
import 'package:udemy_flutter/shared/network/end_points.dart';
import 'package:udemy_flutter/shared/network/remote/dio_helper.dart';

import '../../../models/shop_app/appointment.dart';
import '../../../models/shop_app/booking_model.dart';
import '../../../models/shop_app/comming_appointment.dart';
import '../../../models/shop_app/home_model.dart';
import '../../../models/shop_app/search_model.dart';
import '../../../models/shop_app/types_products_model.dart';
import '../../../shared/components/constants.dart';

class ShopCubit extends Cubit<ShopStates> {
  ShopCubit() : super(ShopInitialState());

  static ShopCubit get(context) => BlocProvider.of(context);

  int currentIndex = 0;

  List<Widget> bottomScreens = [
    HomeScreen(),
    CategoriesScreen(),
    FavritesScreen(),
    DatesScreen(),
    profile_screen(),
  ];

  void changeBottom(int index) {
    currentIndex = index;
    emit(ShopChangeBottomNavState());
  }

  HomeModel homeModel;
  Map<int, dynamic> favorites = {};
  void getHomeData() {
    emit(ShopLoadingHomeDataState());

    DioHelper.getData(
      url: HOME,
      token: token,
    ).then((value) {
      homeModel = HomeModel.fromJson(value.data);
      // homeModel.data.products.forEach((element) {
      //   favorites.addAll({
      //     element.id: element.inFavorites,
      //   });
      // });
      print('favorites');

      print(favorites.toString());

      emit(ShopSuccessHomeDataState());
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorHomeDataState());
    });
  }

  ChangeFavoritesModel changeFavoritesModel;

  void changeFavorites(int productId) {
    favorites[productId] = !favorites[productId];

    emit(ShopChangeFavoritesState());

    DioHelper.postData(
      url: FAVORITE,
      data: {
        'id': productId,
      },
      token: token,
    ).then((value) {
      changeFavoritesModel = ChangeFavoritesModel.fromJson(value.data);
      print(changeFavoritesModel.message.toString());

      if (!changeFavoritesModel.status) {
        favorites[productId] = !favorites[productId];
      } else {
        getFavorites();
      }

      emit(ShopSuccessChangeFavoritesState(changeFavoritesModel));
    }).catchError((error) {
      favorites[productId] = !favorites[productId];

      emit(ShopErrorChangeFavoritesState());
    });
  }

  FavoritesModel favoritesModel;

  void getFavorites() {
    emit(ShopLoadingGetFavoritesState());

    DioHelper.getData(
      url: GETFAVORITE,
      token: token,
    ).then((value) {
      favoritesModel = FavoritesModel.fromJson(value.data);

      print(favorites.toString());
      emit(ShopSuccessGetFavoritesState());
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorGetFavoritesState());
    });
  }

  SearchModel m;
  void getallPic() {
    emit(ShopLoadingGetpState());

    DioHelper.getData(
      url: GETALLPIC,
      token: token,
    ).then((value) {
      m = SearchModel.fromJson(value.data);
      m.data.data.forEach((element) {
        favorites.addAll({
          element.id: element.inFavorites,
        });
      });
      print('favorites1');

      print(favorites.toString());
      emit(ShopSuccessGetpState());
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorGetpState());
    });
  }

  CategoriesModel categoriesModel;

  void getCategories() {
    DioHelper.getData(
      url: GET_CATEGORIES,
      token: token,
    ).then((value) {
      categoriesModel = CategoriesModel.fromJson(value.data);

      emit(ShopSuccessCategoriesState());
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorCategoriesState());
    });
  }

  TypeProductsModel typeModel;

  void gettype(String name) {
    emit(ShopLoadingTypeProductsState());
    DioHelper.postData(
      url: GET_TYPE,
      token: token,
      data: {
        'name': '$name',
      },
    ).then((value) {
      typeModel = TypeProductsModel.fromJson(value.data);
      // typeModel.data.forEach((element) {
      //   favorites.addAll({
      //     element.id: element.inFavorites,
      //   });
      // });

      print('favorites22');

      print(favorites.toString());

      emit(ShopSuccessTypeProductsState());
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorTypeProductsState());
    });
  }

  ShopLoginModel userModel;
  void getUserData() {
    emit(ShopLoadingUserDataState());
    DioHelper.getData(
      url: PROFILE,
      token: token,
    ).then((value) {
      userModel = ShopLoginModel.fromJson(value.data);

      emit(ShopSuccessUserDataState(userModel));
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorUserDataState());
    });
  }

  void updateUserData({
    @required String name,
    // @required String email,
    @required String phone,
  }) {
    emit(ShopLoadingUpdateUserState());

    DioHelper.putData(
      url: UPDATE,
      token: token,
      data: {
        'name': name,
        // 'email': email,
        'phone': phone,
      },
    ).then((value) {
      userModel = ShopLoginModel.fromJson(value.data);
      // printFullText(userModel.data.name);

      emit(ShopSuccessUpdateUserState(userModel));
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorUpdateUserState());
    });
  }

  DateTime focused_Day = DateTime.now();
  DateTime selected_Day;

  void changeDate(x, y) {
    selected_Day = x;
    focused_Day = y;
    postDate();
    emit(ShopEnterDaypickerState());
    print(selected_Day);
  }

  CalendarFormat calendar_Format = CalendarFormat.month;
  void changeCalendarFormat(format) {
    calendar_Format = format;

    emit(ShopCalendarFormatState());
  }

  BookingModel bookedModel;

  void postDate() {
    emit(ShopLoadingGetDateState());
    DioHelper.getData(
      url: GET_BOOKING,
      token: token,
      query: {
        'date': '$selected_Day',
      },
    ).then((value) {
      bookedModel = BookingModel.fromJson(value.data);
      // print(bookedModel.data[0].toString());
      emit(ShopSuccessGetDateState());
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorGetDateState());
    });
  }

  AppointmentModel appointmentModel;
  void creatAppoitment(selectedTime) {
    emit(ShopLoadingBookingState());
    DioHelper.postData(
      url: CREAT_APPOINTMENT,
      token: token,
      data: {
        'date': '$selected_Day',
        'time': '$selectedTime',
      },
    ).then((value) {
      appointmentModel = AppointmentModel.fromJson(value.data);
      getCommApp();
      emit(ShopSuccessBookingState());
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorBookingState());
    });
  }

  bool x = false;
  void changetime() {
    x = !x;

    emit(ShopChangeTimeState());
  }

  void gettoken(String name) {
    emit(ShopLoadingDeviceTokenState());
    DioHelper.postData(
      url: TOKEN_DEVICE,
      token: token,
      data: {
        'device_token': '$name',
      },
    ).then((value) {
      emit(ShopSuccessDeviceTokenState());
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorDeviceTokenState());
    });
  }

  NotificationModel noti;

  void getNotification() {
    emit(ShopLoadingNotificationState());
    DioHelper.getData(
      url: NOTIFICATION,
      token: token,
    ).then((value) {
      noti = NotificationModel.fromJson(value.data);

      emit(ShopSuccessNotificationState());
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorNotificationState());
    });
  }

  bool customTileExpanded = false;
  void changecolapse() {
    customTileExpanded = !customTileExpanded;
    emit(ShopChangecolaState());
  }

  CommingAppModel commingapp;

  void getCommApp() {
    emit(ShopLoadingCommingAppState());
    DioHelper.getData(
      url: COMMING,
      token: token,
    ).then((value) {
      commingapp = CommingAppModel.fromJson(value.data);

      emit(ShopSuccessCommingAppState());
    }).catchError((error) {
      print(error.toString());
      emit(ShopErrorCommingAppState());
    });
  }
}
