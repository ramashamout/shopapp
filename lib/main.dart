// import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:udemy_flutter/layout/shop_app/cubit/cubit.dart';
import 'package:udemy_flutter/modules/shop_app/on_boarding/on_boarding_screen.dart';
import 'package:udemy_flutter/shared/components/bloc_observer.dart';
import 'package:udemy_flutter/shared/cubit/cubit.dart';
import 'package:udemy_flutter/shared/cubit/states.dart';
import 'package:udemy_flutter/shared/network/local/cashe_helper.dart';
import 'package:udemy_flutter/shared/network/remote/dio_helper.dart';
import 'package:udemy_flutter/shared/styles/themes.dart';

import 'modules/shop_app/login/shop_login_screen.dart';

// Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
//   print('on backgroundScreen');
//   // print(message.data.toString());
//   showToast(text: 'on backgroundScreen', state: ToastStates.SUCCESS);

//   print(message.notification.body.toString());
// }

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // firebase
  // await Firebase.initializeApp();

  print("device Token");

  // String tokenFcm = await FirebaseMessaging.instance.getToken();

  // print(tokenFcm);

  // FirebaseMessaging.onMessage.listen((event) {
  //   showToast(text: 'onMessage', state: ToastStates.SUCCESS);

  //   print(event.notification.body.toString());
  // });

  // FirebaseMessaging.onMessageOpenedApp.listen((event) {
  //   showToast(text: 'onMessage', state: ToastStates.SUCCESS);

  //   print(event.notification.body.toString());
  // });
  // FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);

  Bloc.observer = MyBlocObserver();
  DioHelper.init();
  await Cashehelper.init();
  Widget widget;
  bool isDark = Cashehelper.getData(key: 'isDark ');
  bool onBoarding = Cashehelper.getData(key: 'onBoarding');
  String token = Cashehelper.getData(key: 'token');
  print('login Token');
  print(Cashehelper.sharedPreferences.get('token'));
  // String token1 = Cashehelper.getData(key: 'token1');

  if (onBoarding != null) {
    // if (token != null)
    //   widget = ShopLayout();
    // else
    widget = ShopLoginScreen();
  } else {
    widget = OnBoardingScreen();
  }

  runApp(MyApp(isDark: isDark, startWidget: widget));
}

// class MyHttpoverrides extends HttpOverrides {
//   @override
//   HttpClient createHttpClient(SecurityContext context) {
//     return super.createHttpClient(context)
//       ..badCertificateCallback =
//           (X509Certificate cert, String host, int port) => true;
//   }
// }

class MyApp extends StatelessWidget {
  final bool isDark;
  final Widget startWidget;
  final String tokenFcm;
  MyApp({this.isDark, this.startWidget, this.tokenFcm});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => AppCubit()..changeAppMode(fromShared: isDark),
        ),
        BlocProvider(
            create: (context) => ShopCubit()
              ..getallPic()
              ..getHomeData()
              ..getCategories()
              ..getUserData()
              ..gettoken(this.tokenFcm)
              ..getNotification()
              ..getFavorites()
              ..getCommApp()),
      ],
      child: BlocConsumer<AppCubit, Appstates>(
        listener: (context, state) {},
        builder: (context, state) {
          return MaterialApp(
              debugShowCheckedModeBanner: false,
              theme: lightTheme,
              darkTheme: darkTheme,
              themeMode: AppCubit.get(context).isDark
                  ? ThemeMode.light
                  : ThemeMode.light,
              //  AppCubit.get(context).isDark ? ThemeMode.dark : ThemeMode.light,
              home: Directionality(
                  textDirection: TextDirection.rtl, child: startWidget));
        },
      ),
    );
  }
}
