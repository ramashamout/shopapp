class HomeModel {
  bool status;
  HomeDataModel data;

  HomeModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = HomeDataModel.fromJson(json['data']);
  }
}

class HomeDataModel {
  List<BannerModel> banners = [];
  List<ProductModel> products = [];

  HomeDataModel.fromJson(Map<String, dynamic> json) {
    json['banners'].forEach((element) {
      banners.add(BannerModel.fromJson(element));
    });

    json['products'].forEach((element) {
      products.add(ProductModel.fromJson(element));
    });
  }
}

class BannerModel {
  int id;
  String image;

  BannerModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
  }
}

class ProductModel {
  int id;
  String name;
  dynamic selling_price;
  dynamic rent_price;
  dynamic old_price;
  String piece_img;
  String type;
  dynamic discount;
  dynamic inFavorites;
  int size;
  ProductModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    selling_price = json['selling_price'];
    rent_price = json['rent_price'];
    piece_img = json['piece_img'];
    name = json['name'];
    type = json['type'];
    size = json['size'];
    discount = json['discount'];
    old_price = json['old_price'];

    inFavorites = json['inFavorites'];
  }
}
