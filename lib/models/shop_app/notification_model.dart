class NotificationModel {
  bool status;
  String message;
  DataModel data;

  NotificationModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? DataModel.fromJson(json['data']) : null;
  }
}

class DataModel {
  List<noti> notif;

  DataModel.fromJson(Map<String, dynamic> json) {
    if (json['notification'] != null) {
      notif = <noti>[];
      json['notification'].forEach((v) {
        notif.add(new noti.fromJson(v));
      });
    }
  }
}

class noti {
  dynamic title;
  dynamic body;

  noti.fromJson(Map<String, dynamic> json) {
    body = json['body'];
    title = json['title'];
  }
}
