class AppointmentModel {
  bool status;
  String message;
  List<DataModel> data;

  AppointmentModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = <DataModel>[];
      json['data'].forEach((v) {
        data.add(new DataModel.fromJson(v));
      });
    }
  }
}

class DataModel {
  int id;
  String title;
  String color;
  String mobileNumber;
  Null type;
  dynamic date;
  dynamic time;

  DataModel(
      {this.id,
      this.title,
      this.color,
      this.mobileNumber,
      this.type,
      this.date,
      this.time});

  DataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    color = json['color'];
    mobileNumber = json['mobile_number'];
    type = json['type'];
    date = json['date'];
    time = json['time'];
  }
}
