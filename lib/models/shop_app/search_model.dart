class SearchModel {
  bool status;
  String message;
  Data data;

  SearchModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }
}

class Data {
  List<Product> data = [];

  Data.fromJson(Map<String, dynamic> json) {
    json['products'].forEach((element) {
      data.add(Product.fromJson(element));
    });
  }
}

class Product {
  int id;
  String name;
  String pieceImg;
  dynamic sellingPrice;
  dynamic rentPrice;
  String type;
  String designerName;
  String color;
  int size;
  String clothesType;
  dynamic inFavorites;

  dynamic oldPrice;
  dynamic discount;
  Product.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    pieceImg = json['piece_img'];
    sellingPrice = json['selling_price'];
    rentPrice = json['rent_price'];
    type = json['type'];
    designerName = json['designer_name'];
    color = json['color'];
    size = json['size'];
    clothesType = json['clothes_type'];
    inFavorites = json['inFavorites'];
    oldPrice = json['old_price'];
    discount = json['discount'];
  }
}
