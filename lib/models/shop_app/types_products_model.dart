class TypeProductsModel {
  bool status;
  String message;
  List<DataModel> data;

  TypeProductsModel({this.status, this.message, this.data});

  TypeProductsModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    if (json['data'] != null) {
      data = <DataModel>[];
      json['data'].forEach((v) {
        data.add(new DataModel.fromJson(v));
      });
    }
  }
}

class DataModel {
  int id;
  String name;
  String pieceImg;
  dynamic sellingPrice;
  dynamic rentPrice;
  String type;
  String designerName;
  String color;
  int size;
  String clothesType;
  dynamic inFavorites;

  dynamic oldPrice;
  dynamic discount;

  DataModel(
      {this.id,
      this.name,
      this.pieceImg,
      this.sellingPrice,
      this.rentPrice,
      this.type,
      this.designerName,
      this.color,
      this.size,
      this.clothesType,
      this.oldPrice,
      this.discount});

  DataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    pieceImg = json['piece_img'];
    sellingPrice = json['selling_price'];
    rentPrice = json['rent_price'];
    type = json['type'];
    designerName = json['designer_name'];
    color = json['color'];
    size = json['size'];
    clothesType = json['clothes_type'];
    inFavorites = json['inFavorites'];
    oldPrice = json['old_price'];
    discount = json['discount'];
  }
}
