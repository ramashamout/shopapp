class FavoritesModel {
  bool status;
  String message;
  Data data;

  FavoritesModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];

    data = Data.fromJson(json['data']);
  }
}

class Data {
  List<Products> products = [];

  Data.fromJson(Map<String, dynamic> json) {
    json['products'].forEach((v) {
      products.add(new Products.fromJson(v));
    });
  }
}

class Products {
  int id;
  String name;
  int sellingPrice;
  int oldPrice;
  int discount;
  String pieceImg;
  bool inFavorites;
  int rentPrice;
  String type;
  String designerName;
  String color;
  int size;
  String clothesType;

  Products.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    sellingPrice = json['selling_price'];
    oldPrice = json['old_price'];
    discount = json['discount'];
    pieceImg = json['piece_img'];
    inFavorites = json['inFavorites'];
    rentPrice = json['rent_price'];
    type = json['type'];
    designerName = json['designer_name'];
    color = json['color'];
    size = json['size'];
    clothesType = json['clothes_type'];
  }
}
