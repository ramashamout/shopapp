class ShopLoginModel {
  bool status;
  String message;
  LoginModelData data;

  ShopLoginModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? LoginModelData.fromJson(json['data']) : null;
  }
}

class LoginModelData {
  UserData user;
  String token;
  LoginModelData.fromJson(Map<String, dynamic> json) {
    token = json['token'];

    user = json['user'] != null ? UserData.fromJson(json['user']) : null;
  }
}

class UserData {
  dynamic id;
  dynamic name;
  dynamic email;
  dynamic mobileNumber;
  dynamic image;
  dynamic deviceToken;
  dynamic password;
  // named constructor
  UserData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    mobileNumber = json['mobile_number'];

    image = json['profile_photo_path'];

    password = json['password'];

    deviceToken = json['device_token'];
  }
}
