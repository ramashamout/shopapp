class CommingAppModel {
  bool status;
  String message;
  List<DataModel> data = [];

  CommingAppModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];

    json['data'].forEach((v) {
      data.add(new DataModel.fromJson(v));
    });
  }
}

class DataModel {
  dynamic id;

  dynamic type;
  dynamic date;
  dynamic time;
  String statue;

  DataModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];

    // type = json['type'];
    date = json['date'];
    time = json['time'];
    statue = json['statue_app'];
  }
}
