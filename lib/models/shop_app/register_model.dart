class ShopRegisterModel {
  bool status;
  String message;
  RegisterModelData data;

  ShopRegisterModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data =
        json['data'] != null ? RegisterModelData.fromJson(json['data']) : null;
  }
}

class RegisterModelData {
  UserData user;
  String token;
  RegisterModelData.fromJson(Map<String, dynamic> json) {
    token = json['token'];

    user = json['user'] != null ? UserData.fromJson(json['user']) : null;
  }
}

class UserData {
  int id;
  String name;
  String email;
  String mobileNumber;
  String image;
  String password;

  // named constructor
  UserData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    mobileNumber = json['mobile_number'];

    image = json['profile_photo_path'];

    password = json['password'];
  }
}
