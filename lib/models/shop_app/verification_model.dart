class VerificationModel {
  bool status;
  String message;
  String data;

  VerificationModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'];
  }
}
